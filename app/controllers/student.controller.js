let db = require('../models/db');
const Student = db.students;
const Lesson = db.lessons;
const User = db.users;

const jwt = require("../services/auth.services");



exports.getAll = async (req, res) => {
        await jwt.verifyToken(req, res);

    try {
        let result = await Student.findAll();
        res.json(result);
    }
    catch (e) {
        res.status(500)
        res.json({ "message": e });
    }
}

exports.getAllFriends = async (req, res) => {
    userId = await jwt.verifyToken(req, res);
    console.log("here");
    
        try {
            let user = await User.findByPk(userId);
            console.log(user);
            let student = await user.getStudent();
            console.log(student);
            let friends = await student.getFriends();
            console.log(friends);
            res.json(friends);
        } catch (e) {
            res.status(500)
            res.json({ "message": e });
        }
}

exports.addFriend = async (req, res) => {
    userId = await jwt.verifyToken(req, res);
    
    if (req.params.id) {


        try {
            let user = await User.findByPk(userId);
            let student = await user.getStudent();
            let friend = await Student.findByPk(req.params.id);
            if (!friend) {
                res.status(404);
                res.json({ "message": "user not found" });
                return;
            }
            result = await student.addFriend(friend);
            res.json(result);
        }
        catch (e) {
            res.status(500)
            res.json({ "message": e });
        }
    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}

exports.removeFriend = async (req, res) => {
   userId = await jwt.verifyToken(req, res);
    
    if (req.params.id) {


        try {
            let user = await User.findByPk(userId);
            let student = await user.getStudent();
            let friend = await Student.findByPk(req.params.id);
            if (!friend) {
                res.status(404);
                res.json({ "message": "user not found" });
                return;
            }
            result = await student.removeFriend(friend);
            res.json(result);
        }
        catch (e) {
            res.status(500)
            res.json({ "message": e });
        }
    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}

exports.enrollLesson = async (req, res) => {
     userId = await jwt.verifyToken(req, res);
    
    if (req.params.id) {


        try {
            let user = await User.findByPk(userId);
            let student = await user.getStudent();
            let lesson = await Lesson.findByPk(req.params.id);
            if (!lesson) {
                res.status(404);
                res.json({ "message": "lesson not found" });
                return;
            }
            result = await student.addLesson(lesson);
            res.json(result);
        }
        catch (e) {
            res.status(500)
            res.json({ "message": e });
        }
    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}

exports.getById = async (req, res) => {
        await jwt.verifyToken(req, res);

   if (req.params.id) {
       try {
           let result = await Student.findByPk(req.params.id);
           res.json(result);
    
       } catch (e) {   
        res.status(500)
        res.json({ "message": e });
        }
         
    } else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}

