
var express = require('express');
var router = express.Router();
const studentsController = require('../controllers/student.controller');



router.get('/', studentsController.getAll);

router.get('/:id', studentsController.getById);


router.post('/enroll/:id', studentsController.enrollLesson);


module.exports = router;