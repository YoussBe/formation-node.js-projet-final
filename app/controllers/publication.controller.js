let db = require('../models/db');
const Publication = db.publications;
const User = db.users;
const Lesson = db.lessons;

const jwt = require("../services/auth.services");



exports.getAll = async (req, res) => {
    await jwt.verifyToken(req, res);

    try {
        let result = await Publication.findAll();
        console.log(result);
        res.json(result);
    }
    catch (e) {
        res.status(500)
        res.json({ "message": e });
    }
}

exports.getById = async (req, res) => {
        await jwt.verifyToken(req, res);

   if (req.params.id) {
       try {
           let result = await Publication.findByPk(req.params.id);
           if (!result) {
               res.status(404);
               return res.end()

           }
           res.json(result);
    
       } catch (e) {   
        res.status(500)
        res.json({ "message": e });
        }
         
    } else {
        res.json({error: `Veuillez remplir les tous les champs neccessaires`});
    }
}

 exports.update = async (req, res) => {

    let userId = await jwt.verifyToken(req, res);


    if (req.body) {
        try {
            let user = await User.findByPk(userId);
            console.log(user);
            if (user.dataValues.type == 1) {
                let student = await user.getStudent();

                await Publication.update(req.body, {
                    where: {
                        id: req.params.id,
                        studentId : student.dataValues.id,
                    }
                });

         
            } else {
                let teacher = await user.getTeacher();
                  await Publication.update(req.body, {
                    where: {
                        id: req.params.id,
                        teacherId : teacher.dataValues.id,
                    }
                });
            }

            let lesson = {};   //add course part
            if (req.body.lesson) {
                lesson = await result.addLesson(req.body.lesson);
            }
            res.json({ publication: { id: req.params.id, ...req.body }, lesson:lesson });
        } catch (e) {
            res.status(500);
            res.json({ error: e });
        }
               
    } else {
        res.status(400);
        res.json({error: 'bad request'});
    }
} 

exports.create = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);
    if (req.body) {
        try {
            let user = await User.findByPk(userId);
            
            if (parseInt(user.dataValues.type) == 1) {
                 userInfo = await user.getStudent()
            } else {
                 userInfo = await user.getTeacher()
            }
            let result = await userInfo.createPublication(req.body.publication);
            //add course part 
            let found;
            if (req.body.lesson) {
                found = await Lesson.findByPk(req.body.lesson.id);
                await found.addPublication(result);
            }
            res.json({ publication : result, lesson : found });
        } catch (e) {
            res.status(500)
            res.json({ error: e });
        }
    } else {
       res.status(400);
        res.json({error: 'bad request'});
    }
}



exports.remove = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);
    if (req.params.id) {
        
        try {
                 let user = await User.findByPk(userId);
            if (user.dataValues.type == 1) {
                let student = await user.getStudent();

               await Publication.destroy({
                where: {
                       id: req.params.id,
                        studentId : student.dataValues.id
                }
            });
            } else {
                let teacher = await user.getTeacher();
                      await Publication.destroy({
                where: {
                       id: req.params.id,
                        teacherId : teacher.dataValues.id
                }
            });
            }
            res.status(200)
            res.end();

            
        } catch (e) {
        res.status(500)
        res.json({ "message": e });
        }

    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}