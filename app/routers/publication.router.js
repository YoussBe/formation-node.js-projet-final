
var express = require('express');
var router = express.Router();
const publicationController = require('../controllers/publication.controller');



router.get('/', publicationController.getAll);

router.get('/:id', publicationController.getById);

router.post('/', publicationController.create);

router.put('/:id', publicationController.update);

router.delete('/:id',publicationController.remove);


module.exports = router;