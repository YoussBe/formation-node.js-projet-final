
var express = require('express');
var router = express.Router();
const userController = require('../controllers/user.controller');



router.get('/', userController.getInfo);
router.post('/', userController.createInfo);
router.put('/:id', userController.updateInfo);
router.get('/enrolled', userController.getLessons);




module.exports = router;