let db = require('../models/db');
const Teacher = db.teachers;
const jwt = require("../services/auth.services");



exports.getAll = async (req, res) => {
        await jwt.verifyToken(req, res);

    try {
        let result = await Teacher.findAll();
        res.json(result);
    }
    catch (e) {
        res.status(500)
        res.json({ "message": e });
    }
}

exports.getById = async (req, res) => {
        await jwt.verifyToken(req, res);

   if (req.params.id) {
       try {
           let result = await Teacher.findByPk(req.params.id);
           res.json(result);
    
       } catch (e) {   
        res.status(500)
        res.json({ "message": e });
        }
         
    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}

