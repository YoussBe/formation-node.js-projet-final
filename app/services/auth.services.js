const config = require("../config/auth.config");
const jwt = require("jsonwebtoken");


// verifie si le token est validé et envoie un code d'erreur adéquat 
exports.verifyToken = async (req, res) => {
    let token = req.headers['x-access-token'];
    console.log('verify '+token);
    if (!token) {
        res.status(401)
        res.json({ "message":"Unauthorized"})
        return;
    }
    try {
        let response = await jwt.verify(token, config.secret);
        console.log(response);
        return response.id;
    } catch (e) {
        res.status(403)
        res.json({ "message": " Forbidden" })
        return;
    }

}


// genere le token
exports.signToken = (id) => {
    let token = jwt.sign({ id: id }, config.secret, { expiresIn: 3600000 });
    console.log(token);
    return token;
}