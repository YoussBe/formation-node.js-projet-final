let db = require("../models/db");
let jwt = require("../services/auth.services");
let validator = require("../services/validator.services");

let bcrypt = require("bcrypt");
let User = db.users;
let Student = db.students;
let Teacher = db.teachers;

exports.login = async (req, res) => {

    if (validator.validateEmail(req.body.email) && req.body.password) {
        try {
            //trouver le user avec le parametre email == req.body.email
            const user = await User.findOne({ where: { email: req.body.email } });
            if (!user) {
                res.status(404);
                res.json({ "message": "Aucun utilisateur n'existe avec cet email" })
                return;
            }

            if (bcrypt.compareSync(req.body.password, user.dataValues.password)) {
                console.log("password OK")
                //recuperer le student qui est en relation avec notre user : la methode getStudent() est automatiquement genérée par Sequelize suivant la relation défnie auparavant 
                let token = jwt.signToken(user.dataValues.id);
                console.log(token);
                let userInfo = {};
                if (user.dataValues.type == 1) {
                    userInfo = await user.getStudent()
                } else {
                    userInfo = await user.getTeacher()
                }
                res.json({
                    user: {
                
                        id: user.dataValues.id,
                        email: user.dataValues.email,
                        type: parseInt(user.dataValues.type)
                
                    },
                    userInfo: userInfo,
                    token: token,
                });



            } else {
                res.status(401);
                res.json({ "message": "Le mot de passe est erroné" })
            }




        } catch (e) {
            res.status(500);
            res.json({ "message": e })
        }
    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
   
}
exports.register = async (req, res) => {

    if (validator.validateEmail(req.body.email) && req.body.password && req.body.type) {
        
        

        try {

            const user = await User.findOne({ where: { email: req.body.email } });

            if (!user) {
                let hashedPassword = bcrypt.hashSync(req.body.password, 10)
                req.body.password = hashedPassword;

                let result = await User.create(req.body);
                res.json({
                
                    id: result.dataValues.id,
                    email: result.dataValues.email,
                    type: result.dataValues.type
                
                });
            
            } else {
                res.status(409);
                res.json({ "message": 'Cet email est déja utilisé' })
            }

       


        } catch (e) {
            res.status(500);
            res.json({ "message": e })
        }
    }else {
        res.status(400);
        res.json({error: 'bad request'});
    }
   
}

exports.getInfo = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);

    try {
        console.log(userId)
        let user = await User.findByPk(userId);
        console.log(user);
        if (!user) {
            res.status(404);
            res.json({ "message": "Aucun utilisateur n'existe avec cet id" })
            return;
        }
            let userInfo = {};
            if (parseInt(user.dataValues.type) == 1) {
                 userInfo = await user.getStudent()
            } else {
                 userInfo = await user.getTeacher()
            }
            res.json({user : {
                
                    id: user.dataValues.id,
                    email: user.dataValues.email,
                    type : parseInt(user.dataValues.type)
            },
                userInfo : userInfo
            });



        




    } catch (e) {
        res.status(500);
            res.json({ "message": 'database error' })
    }
   
}

exports.getLessons = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);

    try {
        console.log(userId)
        let user = await User.findByPk(userId);
        console.log(user);
        if (!user) {
            res.status(404);
            res.json({ "message": "Aucun utilisateur n'existe avec cet id" })
            return;
        }
        let student = await user.getStudent()
        let lessons = await student.getLessons();
        res.json(lessons);

    } catch (e) {
        res.status(500);
            res.json({ "message": 'database error' })
    }
   
}

exports.createInfo = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);
    try {
        console.log(userId)
        let user = await User.findByPk(userId);
        console.log(user);
        if (!user) {
            res.status(404);
            res.json({ "message": "Aucun utilisateur n'existe avec cet id" })
            return;
        }
        if (parseInt(user.dataValues.type) == 1) {
                //do conditions for bad request
                 userInfo = await user.createStudent(req.body)
            } else {
                 userInfo = await user.createTeacher(req.body)
        }
        console.log(userInfo);
            res.json({user : {
                
                    id: user.dataValues.id,
                    email: user.dataValues.email,
                    type : parseInt(user.dataValues.type)
            },
                userInfo : userInfo
            });
    } catch (e) {
        res.status(500);
            res.json({ "message": 'database error' })
    }
   
}


exports.updateInfo = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);
    let infoId = parseInt(req.params.id);

    try {
        let user = await User.findByPk(userId);
        if (!user) {
            res.status(404);
            res.json({ "message": "Aucun utilisateur n'existe avec cet id" })
            return;
        }
        if (parseInt(user.dataValues.type) == 1) {
                //do conditions for bad request
            await Student.update(req.body, { where: { id: infoId } });
            } else {
            await Teacher.update(req.body, { where: { id: infoId } });

        }
            res.json({user : {
                
                    id: user.dataValues.id,
                    email: user.dataValues.email,
                    type : parseInt(user.dataValues.type)
            },
                userInfo : {"id" :infoId,...req.body}
            });
    } catch (e) {
        res.status(500);
            res.json({ "message": 'database error' })
    }
   
}