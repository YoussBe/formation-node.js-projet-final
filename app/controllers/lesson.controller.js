let db = require('../models/db');
const Lesson = db.lessons;
const User = db.users;
const lessonsService = require("../services/lessons.services");
const jwt = require("../services/auth.services");



exports.getAll = async (req, res) => {
    try {
        let result = await Lesson.findAll();
        console.log(result);
        let newResult = result.map(element => lessonsService.checkFinished(element));
        res.json(newResult);
    }
    catch (e) {
        res.status(500)
        res.json({ "message": e });
    }
}

exports.getById = async (req, res) => {
   if (req.params.id) {
       try {
           let result = await Lesson.findByPk(req.params.id);
           let newResult = lessonsService.checkFinished(result);
           res.json(newResult);
    
       } catch (e) {   
        res.status(500)
        res.json({ "message": e });
        }
         
    } else {
        res.status(400)
        res.json({"message": "bad request"});
    }
}

exports.update = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);

    if (req.body && req.params.id) {
        try {
            let user = await User.findByPk(userId);
            if (parseInt(user.dataValues.type) == 2) {
                 teacher = await user.getTeacher()
            }
            await Lesson.update(req.body, {
                where: {
                    id: req.params.id,
                    teacherId : teacher.dataValues.id,
                }
            });

            res.json({ id: req.params.id, ...req.body });
        } catch (e) {
            res.json(500);
            res.json({ error: e });
        }
               
    } else {
        res.status(400);
        res.json({error: 'bad request'});
    }
}

exports.create = async (req, res) => {

    let userId = await jwt.verifyToken(req, res);

    if (req.body) {

           let user = await User.findByPk(userId);
            if (parseInt(user.dataValues.type) == 2) {
                 userInfo = await user.getTeacher()
            }

        try {
            let result = await userInfo.createLesson(req.body);
            res.json(result);
        } catch (e) {
            res.status(500)
            res.json({ error: e });
        }
         
    } else {
        res.status(400)
        res.json({"message": "bad request"});
    }
}



exports.remove = async (req, res) => {
    let userid = await jwt.verifyToken(req, res);
    if (req.params.id) {
        try {
            await Lesson.destroy({
                where: {
                    id: req.params.id
                }
            });
        } catch (e) {
        res.status(500)
        res.json({ "message": e });
        }

    }
}