let db = require('../models/db');
const Comment = db.comments;
const Publication = db.publications;
const User = db.users;

const jwt = require("../services/auth.services");



exports.getAll = async (req, res) => {
    await jwt.verifyToken(req, res);

    try {
        //trouber les commentaires
        let result = await Comment.findAll();

        //trouvers l'auteur du commentaire
         prom = result.map(async element => {
            if (element.dataValues.teacherId) {
                author = await element.getTeacher();
            } else {
                author = await element.getStudent();
            }
        
            element.dataValues.author = author;
            return element.dataValues;
        });

        const newres = await Promise.all(prom)

        res.json(newres);

    }
    catch (e) {
        res.status(500)
        res.json({ "message": e });
    }
}
exports.getByPublication = async (req, res) => {
    await jwt.verifyToken(req, res);

    if (req.params.id) {

        try {
            let pub = await Publication.findByPk(req.params.id);
            //trouber les commentaires
            let result = await pub.getComments();
             
            
            //trouvers les auteurs des commentaires
            prom = result.map(async element => {
                if (element.dataValues.teacherId) {
                    author = await element.getTeacher();
                } else {
                    author = await element.getStudent();
                }
        
                element.dataValues.author = author;
                return element.dataValues;
            });

            const newres = await Promise.all(prom)
            console.log(newres);
            res.json(newres);
        }
        catch (e) {
            res.status(500)
            res.js
            on({ "message": e });
        }
    } else {
        res.status(400)
        res.json({"message":"Bad request"})

    }
}

exports.getById = async (req, res) => {
        await jwt.verifyToken(req, res);

   if (req.params.id) {
       try {
           let result = await Comment.findByPk(req.params.id);
           if (!result) {
               res.status(404);
               return res.end()

           }
            if (result.dataValues.teacherId) {
                author = await result.getTeacher();
            } else {
                author = await result.getStudent();
            }
        
            result.dataValues.author = author;
      

     
           res.json(result);
    
       } catch (e) {   
        res.status(500)
        res.json({ "message": e });
        }
         
   } else {
        res.status(400)
        res.json({"message": "bad request"});
    }
}


exports.create = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);
    if (req.body.body_text && req.params.id) {
        try {
            let publication = await Publication.findByPk(req.params.id);

            if (!publication) {
                res.status(404)
                res.json({"message":"publication inéxistante"})
                return;
            }

            let user = await User.findByPk(userId);
            if (parseInt(user.dataValues.type) == 1) {
                 userInfo = await user.getStudent()
            } else {
                 userInfo = await user.getTeacher()
            }
            let comment = await userInfo.createComment(req.body);
            await publication.addComment(comment);
           
            res.json( comment);
        } catch (e) {
            res.status(500)
            res.json({ error: e });
        }
    } else {
        res.status(400)
        res.json({"message": "bad request"});
    }
}



exports.remove = async (req, res) => {
    let userId = await jwt.verifyToken(req, res);
    if (req.params.id) {
        
        try {
                 let user = await User.findByPk(userId);
            if (user.dataValues.type == 1) {
                let student = await user.getStudent();

               await Comment.destroy({
                where: {
                       id: req.params.id,
                        studentId : student.dataValues.id
                }
            });
            } else {
                let teacher = await user.getTeacher();
                      await Comment.destroy({
                where: {
                       id: req.params.id,
                        teacherId : teacher.dataValues.id
                }
            });
            }
            res.status(200)
            res.end();

            
        } catch (e) {
        res.status(500)
        res.json({ "message": e });
        }

    }else {
        res.status(400)
        res.json({"message": "bad request"});
    }
}