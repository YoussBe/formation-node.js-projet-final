
var express = require('express');
var router = express.Router();
const teacherController = require('../controllers/teacher.controller');



router.get('/', teacherController.getAll);

router.get('/:id', teacherController.getById);



module.exports = router;