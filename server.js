
// importer body-parser et express
var express = require('express');
var bodyParser = require('body-parser');
const cors = require("cors");
const db = require("./app/models/db");
var lessons = require('./app/routers/lesson.router');
var auth = require('./app/routers/auth.router');
var users = require('./app/routers/user.router');
var publications = require('./app/routers/publication.router');
var students = require('./app/routers/student.router');
var teachers = require('./app/routers/teacher.router');
var comments = require('./app/routers/comment.router');
var friends = require('./app/routers/friendship.router');

//créer une application express
let app = express();

var corsOptions = {
  origin: "http://localhost:4200"
};

//ajout de CORS pour permettre à l'application front en local de consomme l'API
app.use(cors(corsOptions));
app.use(function(req, res, next) {
    res.header(
      "Access-Control-Allow-Headers",
      "x-access-token, Origin, Content-Type, Accept"
    );
    next();
});
//ajouter bodyParser comme middleware

app.use(bodyParser.json())

db.sequelize.sync();

//lancer le serveur sur le port 3000
app.listen(3000);

app.use('/lessons', lessons);
app.use('/auth', auth);
app.use('/me', users);
app.use('/publications', publications);
app.use('/students', students);
app.use('/teachers', teachers);
app.use('/comments', comments);
app.use('/friendship', friends);
