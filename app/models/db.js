const Sequelize = require('sequelize');
const dbConfig = require("../config/db.config");


const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.DIALECT,
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;
db.students = require("./student.model")(sequelize, Sequelize);
db.lessons = require("./lesson.model")(sequelize, Sequelize);
db.users = require("./user.model")(sequelize, Sequelize);
db.teachers = require("./teacher.model")(sequelize, Sequelize);
db.publications = require("./publication.model")(sequelize, Sequelize);
db.comments = require("./comment.model")(sequelize, Sequelize);


//********Relationships********//



//créer la relation One-to-one entre User et Student
db.students.hasOne(db.users);
db.users.belongsTo(db.students);
//créer la relation One-to-one entre User et Teacher
db.teachers.hasOne(db.users);
db.users.belongsTo(db.teachers);
//One-to-Many student / publication
db.students.hasMany(db.publications);
db.publications.belongsTo(db.students);
//One-to-Many student / comment
db.students.hasMany(db.comments);
db.comments.belongsTo(db.students);
//Many-to-Many student / student
db.students.belongsToMany(db.students, { as: "Friends", through: 'StudentFriend' });
//Many-to-Many student / lesson
db.students.belongsToMany(db.lessons, { through: 'LessonStudents' });
db.lessons.belongsToMany(db.students, { through: 'LessonStudents' });
//One-to-Many teacher / publication
db.teachers.hasMany(db.publications);
db.publications.belongsTo(db.teachers);
//One-to-Many teacher / publication
db.teachers.hasMany(db.comments);
db.comments.belongsTo(db.teachers);
//Many-to-Many student / lesson
db.teachers.belongsToMany(db.lessons, { through: 'LessonsTeachers' });
db.lessons.belongsToMany(db.teachers, { through: 'LessonsTeachers' });
//One-to-Many teacher / publication
db.publications.hasMany(db.comments);
db.comments.belongsTo(db.publications);
//One-to-Many Lesson / publication
db.lessons.hasMany(db.publications);
db.publications.belongsTo(db.lessons);

module.exports = db;