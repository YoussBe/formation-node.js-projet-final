
var express = require('express');
var router = express.Router();
const studentsController = require('../controllers/student.controller');

router.post('/:id', studentsController.addFriend);
router.get('/', studentsController.getAllFriends);
router.delete('/:id', studentsController.removeFriend);
module.exports = router;