
var express = require('express');
var router = express.Router();
const commentController = require('../controllers/comment.controller');



router.get('/', commentController.getAll);

router.get('/:id', commentController.getById);
router.get('/publication/:id', commentController.getByPublication);

router.post('/publication/:id', commentController.create);

router.delete('/:id',commentController.remove);


module.exports = router;